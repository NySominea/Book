@extends('master')

@section('title')
    CreateBook
@endsection

@section('content')
    <div class='row'>
        <h3>Add a new Book</h3>
        <form action="/" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="usr">Title:</label>
                <input type="text" class="form-control" name='title'>
            </div>
            <div class="form-group">
                <label for="desc">Description:</label>
                <textarea class="form-control" rows="5" name="description"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
@endsection