@extends('master')

@section('title')
    DisplayBooks
@endsection

@section('content')
    <h3>All Books</h3>
    <a href="/create" class='btn btn-primary'>Add New</a>
    <table class='table table-striped'>
        <thead>
            <tr>
                <th>ID</th> <th>Title</th> <th>Description</th>
            </tr>
        </thead>
        <tbody>
            @foreach($books as $book)
            <tr>
                <td>{{$book->id}}</td>
                <td>{{$book->title}}</td>
                <td>{{$book->description}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection